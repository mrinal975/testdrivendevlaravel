<?php

namespace App\Http\Controllers\Task;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Task\Task;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;

class TaskController extends Controller
{
    public function index(Request $request){
        $task = Task::all();
        return response($task);
    }

    public function show($id)
    {
        $task = Task::findOrFail($id);
        return response($task);
    }

    public function store(Request $request)
    {
        $request->validate(['title'=>['required']]);
        $task = Task::create($request->all());
        return response($task, Response::HTTP_CREATED);
    }

    public function destroy($id){
        Task::where('id',$id)->delete();
        return response('', Response::HTTP_NO_CONTENT);
    }

    public function update($id, Request $request){
        $task = Task::where('id',$id)->update(['title'=>$request->title]);
        return response($task, Response::HTTP_CREATED);
    }
}
