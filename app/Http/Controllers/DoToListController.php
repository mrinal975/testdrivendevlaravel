<?php

namespace App\Http\Controllers;

use App\Http\Requests\ToDoListRequest;
use App\Models\TodoList\TodoList;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class DoToListController extends Controller
{
    public function index(Request $request)
    {
        $list = TodoList::all();
        return response($list);
    }

    public function show($id)
    {
        $todoList = TodoList::findOrFail($id);
        return response($todoList);
    }

    public function store(Request $request)
    {
        $request->validate(['name'=>['required']]);
        $todoList = TodoList::create($request->all());
        return response($todoList, Response::HTTP_CREATED);
    }

    public function destroy($id){
        TodoList::where('id',$id)->delete();
        return response('',Response::HTTP_NO_CONTENT);
    }

    public function update($id, ToDoListRequest $request){
        $request->validate(['name'=>['required']]);
        $todoList = TodoList::where('id',$id)->update(['name'=>$request->name]);
        return response($todoList, Response::HTTP_CREATED);
    }
}
