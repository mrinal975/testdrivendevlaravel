<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Models\Task\Task;
use Tests\TestCase;

class TaskTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    private $taskList;

    public function setUp():void
    {
        parent::setUp();
        $this->taskList = Task::factory()->create(
            ['title'=>'baba loknath']
        );
    }

    public function test_fetch_all_task_of_todo_list()
    {
        //preparation

        //action
        $response = $this->getJson(route('task.index'))
                    ->assertOk()
                    ->json();
        //assertion
        // $this->assertEquals(1, count($response));
        $this->assertEquals($response[0]['title'], $this->taskList->title);
    }

    public function test_get_single_task()
    {
        $response = $this->getJson(route('task.show',$this->taskList->id))
                    ->assertOk()
                    ->json();
        $this->assertEquals($response['title'], $this->taskList->title);
    }

    public function test_task_store_for_todo_list()
    {
        $response = $this->postJson(route('task.store'),
                    [
                        'title'=>'baba Loknath title'
                    ])
                    ->assertCreated();
        $this->assertEquals('baba Loknath title',$response['title']);
        $this->assertDatabaseHas('tasks',[
            'title'=>'baba Loknath title'
        ]);

    }

    public function test_delete_task_for_todo_list()
    {
        $this->deleteJson(route('task.destroy',$this->taskList->id))
                    ->assertNoContent();
        // $this->assertDatabaseMissing('tasks',
        // [
        //     'title'=>$this->taskList->title
        // ]);
    }

    public function test_update_task_for_todo_list(){

        $this->patchJson(route('task.update',$this->taskList->id),
        [
            'title'=>'update title baba loknath'
        ])
        ->assertCreated();

        $this->assertDatabaseHas('tasks',[
            'id'=>$this->taskList->id,
            'title'=>'update title baba loknath'
        ]);
    }
}
