<?php

namespace Tests\Feature;


use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Models\TodoList\TodoList;
use Tests\TestCase;
use Database\Factories\TodoList\ToDoListFactory;
class TodoListTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    private $list;

    public function setUp():void
    {
        parent::setUp();
        $this->list = TodoList::factory()->create(
            ['name'=>'baba loknath']
        );
    }

    public function test_fetch_all_todo_list()
    {
        //preparation

        //action
        $response = $this->get('api/todo-list');

        //assertion
        $this->assertEquals($this->list->name, $response->json()[0]['name']);
    }

    public function test_fetch_single_todo_list()
    {
        //preparation

        //action
        $response = $this->getJson(route('todo-list.show',$this->list->id))
                    ->assertOk()
                    ->json();

        // dd($response,$this->list->id);
        //assertion
        $this->assertEquals($response['name'],$this->list->name);

    }

    public function test_store_new_todo_list()
    {
        //preparation

        //action

        $response = $this->postJson(route('todo-list.store',
                    [
                        'name'=>'My todo list'
                    ]))
                    ->assertCreated();

        //assertion
        $this->assertEquals('My todo list',$response['name']);
        $this->assertDatabaseHas('todo_lists',['name'=>'My todo list']);

    }

    public function test_store_validation_todo_list_name_field()
    {
        //preparation

        //action

        $this->withExceptionHandling();
        $response = $this->postJson(route('todo-list.store'))
                    ->assertUnprocessable()
                    ->assertJsonValidationErrors(['name']);
        //assertion

    }

    public function test_delete_todo_list()
    {
        //preparation

        //action

        $this->deleteJson(route('todo-list.destroy',$this->list->id))
                ->assertNoContent();

        $this->assertDatabaseMissing('todo_lists',['name'=>$this->list->name]);
        //assertion

    }

    public function test_update_todo_list()
    {
        $this->patchJson(route('todo-list.update',$this->list->id),[
            'name'=>'baba loknath update'
        ])->assertCreated();

        $this->assertDatabaseHas('todo_lists',[
            'id'=>$this->list->id,
            'name'=>'baba loknath update',
        ]);

    }
}
